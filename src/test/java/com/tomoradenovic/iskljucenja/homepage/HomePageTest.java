package com.tomoradenovic.iskljucenja.homepage;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import javax.sql.rowset.serial.SQLOutputImpl;

public class HomePageTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public HomePageTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     *
     */
    public static Test suite()
    {
        return new TestSuite( HomePageTest.class );
    }

    public void testHomepage()
    {

        String webdriverLocation = System.getProperty("webdriverlocation", "C:\\chromedriver\\chromedriver.exe");
        System.out.println(webdriverLocation);
        System.setProperty("webdriver.chrome.driver", webdriverLocation);

        ChromeOptions chromeOptions = new ChromeOptions();

        // We get the value of "my_headless" system property.
        // In order to run in headless mode, run maven command as follows:
        // mvn -Dmy_headless=true test
        String headless = System.getProperty("my_headless", "false");

        System.out.println("value of my_headless is: " + headless);

        if (headless.equals("true")) {
            chromeOptions.addArguments("--headless");
        }

        //Step 1- Driver Instantiation: Instantiate driver object as FirefoxDriver
        WebDriver driver = new ChromeDriver(chromeOptions);

        //Step 2- Navigation: Open a website
        driver.navigate().to("https://iskljucenja.rs/");

        //Step 3- Assertion: Check its title is correct
        //assertEquals method Parameters: Expected Value, Actual Value, Assertion Message
        assertEquals("Title check failed!", "Iskljucenja.rs - Saznajte kada nećete imati struju. Unapred.", driver.getTitle());

        //Step 4- Close Driver
        driver.close();

        //Step 5- Quit Driver
        driver.quit();
    }

    public void testlogin()
    {
        String webdriverLocation = System.getProperty("webdriverlocation", "C:\\chromedriver\\chromedriver.exe");
        System.out.println(webdriverLocation);
        System.setProperty("webdriver.chrome.driver", webdriverLocation);

        ChromeOptions chromeOptions = new ChromeOptions();

        // We get the value of "my_headless" system property.
        // In order to run in headless mode, run maven command as follows:
        // mvn -Dmy_headless=true test
        String headless = System.getProperty("my_headless", "false");

        System.out.println("value of my_headless is: " + headless);

        if (headless.equals("true")) {
            chromeOptions.addArguments("--headless");
        }

        //Step 1- Driver Instantiation: Instantiate driver object as FirefoxDriver
        WebDriver driver = new ChromeDriver(chromeOptions);

        //Step 2- Navigation: Open a website
        driver.navigate().to("https://iskljucenja.rs/");

        String email= "test123@mailinator.com";
        String password = "Test12!";


        //Step 3- find e-mail field and fill it in
        driver.findElement(By.cssSelector("input[type = text]")).sendKeys(email);
        //Step 4 - find password and fill it in
        driver.findElement(By.cssSelector("input[type=password]")).sendKeys(password);
        //Step 5 - click on the "Prijavite se " button
        driver.findElement(By.cssSelector("button[type=submit]")).submit();
        //Step 6 Wait for the web page to load and assert if the text "Dobrodošli" is visible
        String actualText = driver.findElement(By.className("dashboard_page__wellcome_message")).getText();
        String expectedText = "Dobrodošli";
        assertEquals("Incorrect welcome message", expectedText, actualText);

        //Step 4- Close Driver
        driver.close();

        //Step 5- Quit Driver
        driver.quit();
    }


}
