package com.tomoradenovic.iskljucenja.homepage;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import okhttp3.*;

import java.io.IOException;
import java.util.UUID;

public class RegistrationTest extends TestCase {

    public RegistrationTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(RegistrationTest.class);
    }

    public void testRegistration() throws IOException {

        UUID uuid = UUID.randomUUID();
        String email = "email=" + uuid + "%40mailinator.com";

        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, email + "&password=&confirm_password=&language=srpski-latinica");
        Request request = new Request.Builder()
                .url("https://iskljucenja.rs/localSignUp?language=srpski-latinica")
                .post(body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .build();

        Response response = client.newCall(request).execute();

        System.out.println(email);

        String html = response.body().string();
        System.out.println(html);
        int responseCode = response.code();

        assertEquals("Registration without a password was not successful", 200, responseCode);
        assertTrue("Registration confiramtion e-mail was not sent", html.contains("E-mail za potvrdu prijave na naš servis poslat je na vašu adresu."));
    }


}
